import tkinter as tk
from tkinter import messagebox

# Define login credentials (you can replace these with your own)
valid_credentials = {"user1": "password1", "user2": "password2", "admin": "admin"}

# Create a dictionary to store candidate names and their vote counts
candidates = {"Candidate A": 0, "Candidate B": 0, "Candidate C": 0}

# Create a dictionary to store user details and their voted candidates
user_details = {}

# Create a global variable for the current user
current_user = None

# Function to handle the voting process
def vote(candidate):
    global current_user
    if current_user in user_details:
        if user_details[current_user]["voted"]:
            messagebox.showerror("Error", "You have already voted for a candidate.")
        else:
            candidates[candidate] += 1
            user_details[current_user]["voted"] = candidate
            messagebox.showinfo("Success", f"You have voted for {candidate}. Thank you!")
            voted_label.config(text="Vote Cast: Yes")
           
    else:
        messagebox.showerror("Error", "You must log in to vote.")

# Function to handle login
def login():
    global current_user
    username = username_entry.get()
    password = password_entry.get()
    if (username, password) in valid_credentials.items():
        current_user = username
        if username not in user_details:
            user_details[current_user] = {"voted": None}
        display_home_page()
    else:
        messagebox.showerror("Login Error", "Invalid username or password.")

# Function to display the home page with voting and analysis options
def display_home_page():
    login_window.destroy()
    
    root = tk.Tk()
    root.title("Online Voting System")

    # Voting options
    vote_buttons = []
    for i, candidate in enumerate(candidates, start=1):
        vote_button = tk.Button(root, text=f"Vote for {candidate}", command=lambda c=candidate: vote(c))
        vote_button.place(x=20, y=20 + (i - 1) * 30)
        vote_buttons.append(vote_button)

    # Analysis button
    analyze_button = tk.Button(root, text="Analyze Votes", command=show_analysis)
    analyze_button.place(x=20, y=20 + len(candidates) * 30)

    # Vote Analysis Display
    global voted_label
    voted_label = tk.Label(root, text="Vote Cast: No")
    voted_label.place(x=20, y=20 + (len(candidates) + 1) * 30)

    root.mainloop()

# Function to display the vote counts in a separate window
def show_analysis():
    analysis_window = tk.Toplevel()
    analysis_window.title("Vote Analysis")
    
    analysis_text = tk.Text(analysis_window, height=5, width=30)
    analysis_text.place(x=20,y=10)

    analysis_text.config(state=tk.NORMAL)
    for candidate, count in candidates.items():
        analysis_text.insert(tk.END, f"{candidate}: {count} votes\n")
    analysis_text.config(state=tk.DISABLED)

# Create a login window
login_window = tk.Tk()
login_window.title("Login")
login_label = tk.Label(login_window, text="Please log in")
login_label.place(x=20 ,y =20)
username_label = tk.Label(login_window, text="Username:")
username_label.place(x=20,y=40)
username_entry = tk.Entry(login_window)
username_entry.place(x=100,y=40)
password_label = tk.Label(login_window, text="Password:")
password_label.place(x=20,y=60)
password_entry = tk.Entry(login_window, show="*")
password_entry.place(x=100,y=60)
login_button = tk.Button(login_window, text="Login", command=login)
login_button.place(x=80,y=80)

login_window.mainloop()
